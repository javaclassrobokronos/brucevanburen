package assignment1;

public class Farm {
	public Farm() {
		example1.BarnYard by;
		by = new example1.BarnYard();
		// 1 chicken
		example1.Chicken ch1;
		ch1 = new example1.Chicken();
		by.addChicken(ch1);
		// 2 chickens moving
		example1.Chicken ch2;
		ch2 = new example1.Chicken();
		by.addChicken(ch2);
		ch2.start();
		example1.Chicken ch3;
		ch3 = new example1.Chicken();
		by.addChicken(ch3);
		ch3.start();
		// 2 pigs moving
		example1.Pig pg1;
		pg1 = new example1.Pig();
		by.addPig(pg1);
		pg1.start();
		example1.Pig pg2; //moving
		pg2 = new example1.Pig();
		by.addPig(pg2);
		pg2.start();
		// 1 butterfly
		example1.Butterfly bf;
		bf = new example1.Butterfly();
		by.addButterfly(bf);
	}
}
