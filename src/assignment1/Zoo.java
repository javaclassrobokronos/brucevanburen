package assignment1;

public class Zoo {
	public Zoo() {
		// 3 barnyards
		example1.BarnYard by1;
		by1 = new example1.BarnYard();
		// 1 chicken
		example1.Chicken ch1;
		ch1 = new example1.Chicken();
		by1.addChicken(ch1);
		// 1 chicken moving
		example1.Chicken ch2;
		ch2 = new example1.Chicken();
		by1.addChicken(ch2);
		ch2.start();
		//
		// by2, 3 bf moving
		example1.BarnYard by2;
		by2 = new example1.BarnYard();
		example1.Butterfly bf1;
		bf1 = new example1.Butterfly();
		by2.addButterfly(bf1);
		bf1.start();
		example1.Butterfly bf2;
		bf2 = new example1.Butterfly();
		by2.addButterfly(bf2);
		bf2.start();
		example1.Butterfly bf3;
		bf3 = new example1.Butterfly();
		by2.addButterfly(bf3);
		bf3.start();
		//
		example1.BarnYard by3;
		by3 = new example1.BarnYard();		
		// 1 pig & 1 pig moving
		example1.Pig pg1;
		pg1 = new example1.Pig();
		by3.addPig(pg1);
		pg1.start();
		example1.Pig pg2;
		pg2 = new example1.Pig();
		by3.addPig(pg2);
	}
}
