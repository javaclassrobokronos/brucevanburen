//Try to name all the parts of this class.

package firstClassReview; // package header

public class ExampleCode { // class header
	
	private example1.BarnYard _by; // instance variable
	
	public ExampleCode(){ // constructor header
		_by = new example1.BarnYard(); // constructor body
		addTwoChickens();
	}
	
	public void addTwoChickens(){ // method header
		example1.Chicken c1; // method body
		c1 = new example1.Chicken();
		example1.Chicken c2;
		c2 = new example1.Chicken();
		_by.addChicken(c1);
		_by.addChicken(c2);
		c1.start();
		c2.start();
	}
}
