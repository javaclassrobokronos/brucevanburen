package control;

import ai.AI;
import data.DataBoard;
import data.Tile.Value;
import graphics.Board;

public class Game {
	private OutputPanel _control;
	private DataBoard _data;
	private Board _board;
	private AI _ai;
	private boolean _twoPlayer;
	private boolean _firstPlayersTurn;
	private static Game _game;
	public Game(boolean twoPlayer){
		_twoPlayer = twoPlayer;
		if(!twoPlayer) _ai = new AI();
		else _ai = null;
		_control = new OutputPanel();
		_data = new DataBoard();
		_firstPlayersTurn = true;
		_board = new Board();
		_game = this;
		
	}
	public void triggerNextTurn(){
		checkEndGame();
		_firstPlayersTurn = !_firstPlayersTurn;
		if(!_twoPlayer && !_firstPlayersTurn){
			//_ai.makeMove();
		}
	}
	public static Game getInstance(){
		return _game;
	}
	public boolean getFirstPlayerTurn(){
		return _firstPlayersTurn;
	}
	public void updateGUI(){
		_board.updateGUI();
		//need to update control panel
		OutputPanel.getInstance().changeTurn(!_firstPlayersTurn);
	}
	public void checkEndGame(){
		for(int x = 0; x < 12; x++){
			for(int y = 0; y < 12; y++){
				for(int xd = -1; xd <= 1; xd++){
					for(int yd = -1; yd <=1; yd++){
						if(xd == 0 && yd == 0){
						}
						else{
							Value tileType = consecCheck(x,y,xd,yd);
							if(tileType != Value.None){
								EndGame(tileType);
							}
						}
					}
				}
			}
		}
	}
	public Value consecCheck(int xStart, int yStart, int xd, int yd){
		Value checkingType = DataBoard.getInstance().getTileValue(xStart, yStart);
		if(checkingType != Value.None){
			int xdt = xd;
			int ydt = yd;
			int consec = 1;
			for(int x = 0; x < 4; x++){
				if(xStart + xdt <= 11 && xStart + xdt >= 0 && yStart + ydt <= 11 && yStart + ydt >= 0){
					if(checkingType == DataBoard.getInstance().getTileValue(xStart + xdt, yStart + ydt)){
						xdt += xd;
						ydt += yd;
						consec++;
						if(consec == 5){
							return checkingType;
						}
					}
					else{
						return Value.None;
					}
				}
				else {
					return Value.None;
				}
			}
		}
		else{
			return Value.None;
		}
		return Value.None;
	}
	public void EndGame(Value v){
		_board.disable();
		OutputPanel.getInstance().printEndGame(v);
	}
	
}
