package control;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import data.Tile.Value;

public class OutputPanel {
	private JFrame _frame;
	private static OutputPanel _out;
	private JPanel _panel;
	private JLabel _label;
	private static OutputPanel out;
	public OutputPanel(){
		_out = this;
		_frame = new JFrame("Info Panel");
		_panel = new JPanel();
		_label = new JLabel();
		_frame.add(_panel);
		_panel.add(_label);
		_frame.setSize(400, 200);
		_frame.setBounds(800, 0, 400, 200);
		_frame.setVisible(true);
		out = this;
		_label.setText("It is Player 1(X) turn");
	}
	public void printEndGame(Value v){
		if(v == Value.X){
			_label.setText("Player 1 (X) wins!");
		}
		else{
			_label.setText("Player 2 (O) wins!");
		}
	}
	public void changeTurn(boolean b){
		if(b){
			_label.setText("It is Player 1(X) turn");
		}
		else{
			_label.setText("It is Player 2(O) turn");
		}
	}
	public static OutputPanel getInstance(){
		return out;
	}
}
