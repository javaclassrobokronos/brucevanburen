package control;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;

public class StartMenu {
	private JFrame _frame;
	private JButton _twoPlayer;
	private JButton _computer;
	private Game _game;
	public StartMenu(){
		_frame = new JFrame("Five in a Row!");
		_frame.setLayout(new GridLayout(2, 1));
		_twoPlayer = new JButton("Start a two player game");
		_twoPlayer.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				_game = new Game(true);
				_frame.dispose();
			}
		});
		_computer = new JButton("Start a game against the computer");
		_computer.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				_game = new Game(false);
				_frame.dispose();
			}
		});
		_frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		_frame.add(_computer);
		_frame.add(_twoPlayer);
		_frame.setVisible(true);
		_frame.pack();
	}
}
