package data;

public class Tile {
	private int _x;
	private int _y;
	private Value _val;
	public enum Value{X,O,None};
	public Tile (int x, int y){
		_x = x;
		_y = y;
		_val = Value.None;
	}
	public void mark(Value v){
		if(_val == Value.None) _val = v;
	}
	public Value getValue(){
		return _val;
	}
}
