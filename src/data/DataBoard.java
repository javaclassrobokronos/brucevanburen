package data;

import java.util.ArrayList;

import data.Tile.Value;

public class DataBoard {
	private static DataBoard _dataBoard;
	private ArrayList<ArrayList<Tile>> _tiles;
	public DataBoard(){
		_dataBoard = this;
		_tiles = new ArrayList<ArrayList<Tile>>();
		for(int x = 0; x < 12; x++){
			_tiles.add(new ArrayList<Tile>());
			for(int y = 0; y < 12; y++){
				_tiles.get(x).add(new Tile(x,y));
			}
		}
	}
	
	
	
	public void markX(int x, int y){
		_tiles.get(x).get(y).mark(Value.X);
	}
	
	public void markO(int x, int y){
		_tiles.get(x).get(y).mark(Value.O);
	}
	
	public static DataBoard getInstance(){
		return _dataBoard;
	}
	public Value getTileValue(int x, int y){
		return _tiles.get(x).get(y).getValue();
	}
}
