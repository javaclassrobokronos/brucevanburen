package ai;

import java.awt.Point;
import java.util.ArrayList;

import control.Game;
import data.DataBoard;
import data.Tile.Value;

public class AI {
	// must end with a doClick
	private Game _game;
	private DataBoard _data;
	private ArrayList<ArrayList<Inquiry>> _brain;
	public AI(){
		_brain = new ArrayList<ArrayList<Inquiry>>();
		_game = Game.getInstance();
		_data = DataBoard.getInstance();
		for(int x = 0; x < 12; x++){
			ArrayList<Inquiry> arr = new ArrayList<Inquiry>();
			_brain.add(arr);
			for(int y = 0; y < 12; y++){
				arr.add(new Inquiry(x,y));
				
			}
		}
	}
	
	public void makeMove(){
		evaluateBoard();
	}
	public void evaluateBoard(){
		for(int x = 0; x < 12; x++){
			for(int y = 0; y < 12; y++){
				if(_data.getTileValue(x, y) == Value.None){
					directionalCheck(x,y);
				}
			}
		}
	}
	public void directionalCheck(int xStart, int yStart){
		for(int x = 0; x <= 1; x++){
			for(int y = -1; y <= 1; y++){
				if(!(x == 0 && y == 0) || !(x == 0 && y == -1)){
					if(_data.getTileValue(xStart + x, yStart + y) != Value.None){
						Value checking = _data.getTileValue(xStart + x, yStart + y);
						int distance = distanceCheck(xStart + x,yStart + y,x,y, checking);
						
					}
				}
			}
		}
	}
	//this will start after the first end point and return the distance
	//index 0 is the distance, index 1 and 2 is the point of the gap. -1 if no gap;
	public int[] distanceCheck(int xStart,int yStart, int dx, int dy, Value checking){
		int dxt = dx;
		int dyt = dy;
		boolean gap = false;
		Point gapPoint = new Point(-1,-1);
		int length = 1;
		for(int i = 0; i < 3; i++){
			if(_data.getTileValue(xStart + dxt, yStart + dyt) == checking){
				dxt += dx;
				dyt += dy;
				length++;
			}
			else if(gap == false&&_data.getTileValue(xStart + dxt, yStart + dyt) == Value.None){
				gap = true;
				dxt += dx;
				dyt += dy;
				
			}
		}
	}
	public void resetBrain(){
		for(int x = 0; x < 12; x++){
			for(int y = 0; y < 12; y++){
				_brain.get(x).get(y).reset();
			}
		}
	}

}
