package ai;

public class Inquiry {
	private int _Oval;
	private int _Dval;
	private int _x;
	private int _y;
	public Inquiry(int x, int y){
		_x = x;
		_y = y;
	}
	public void reset(){
		_Oval = 0;
		_Dval = 0;
		
	}
	public void addO(int O){
		_Oval += O;
	}
	public void addD(int D){
		_Dval += D;
	}
}
