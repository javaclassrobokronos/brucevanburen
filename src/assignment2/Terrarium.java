package assignment2;

public class Terrarium {
	public Terrarium() {
	// 2 BarnYards
	example1.BarnYard by1 ;
	by1 = new example1.BarnYard();
	// One BarnYard must have 3 moving and 5 unmoving critters in it.
	example1.Pig pg1;
	pg1 = new example1.Pig();
	by1.addPig(pg1);
	pg1.start();
	// The other must have 4 moving and 2 unmoving critters in it.
	example1.BarnYard by2 ;
	by2 = new example1.BarnYard();	
	example1.Pig pg2;
	pg2 = new example1.Pig();
	by2.addPig(pg2);
	pg2.start();
	example1.Pig pg3;
	pg3 = new example1.Pig();
	by2.addPig(pg3);
	pg3.start();
	}
}
