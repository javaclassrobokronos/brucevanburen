package graphics; 

import java.awt.ComponentOrientation;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;

import control.Game;
import data.DataBoard;
import data.Tile.Value;

public class Board {
	private JFrame _frame;
	private ArrayList<ArrayList<JButton>> _buttons;
	public Board(){
		_buttons = new ArrayList<ArrayList<JButton>>();
		for(int i = 0; i < 12; i++){
			_buttons.add(new ArrayList<JButton>());
			for(int k = 0; k < 12; k++){
				JButton b = new JButton();
				b.setFont(b.getFont().deriveFont(Font.BOLD, b.getFont().getSize() * 3));
				b.addActionListener(new EventHandler(i,k,this));
				_buttons.get(i).add(b);
			}
		}
		_frame = new JFrame("Five in a Row!");
		_frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		_frame.applyComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
		_frame.setLayout(new GridLayout(12,12));
		//must add buttons from different arraylists first
		for (int x = 0; x < 12; x++){
			for(int y = 0; y < 12; y++){
				_frame.add(_buttons.get(y).get(x));
			}
		}
		_frame.setVisible(true);
		_frame.setSize(800, 800);
	}
	public void updateGUI(){
		for(int x = 0; x < 12; x++){
			for(int y = 0; y < 12; y++){
				Value v = DataBoard.getInstance().getTileValue(x, y);
				if(v == Value.X){
					_buttons.get(x).get(y).setText("X");
				}
				else if(v == Value.O){
					_buttons.get(x).get(y).setText("O");
				}
			}
		}
	}
	public void disable(){
		for(int x = 0; x < 12; x++){
			for(int y = 0; y <12; y++){
				_buttons.get(x).get(y).setEnabled(false);
			}
		}
	}
}
