package graphics;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import control.Game;
import data.DataBoard;
import data.Tile.Value;

public class EventHandler implements ActionListener{
	private int _x;
	private int _y;
	private Board _b;
	private DataBoard _data;
	public EventHandler(int x, int y, Board b){
		_x = x;
		_y = y;
		_b = b;
		_data = DataBoard.getInstance();
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if(DataBoard.getInstance().getTileValue(_x, _y) == Value.None){
		if(Game.getInstance().getFirstPlayerTurn()) _data.markX(_x, _y);
		else _data.markO(_x, _y);
		Game.getInstance().updateGUI();
		Game.getInstance().triggerNextTurn();
		}
	}
}
